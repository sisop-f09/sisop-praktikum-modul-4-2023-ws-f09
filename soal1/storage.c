#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void download(char *link);
void unzip(char *source);
void scan();

int main()
{
    pid_t pid1, pid2, pid3;
    int status;

    //pip install kaggle
    char *kaggle = "kaggle datasets download -d bryanb/fifa-player-stats-database";
    char *source = "fifa-player-stats-database.zip";

    // Fungsi download() dijalankan oleh child process pertama
    pid1 = fork();
    if (pid1 == 0)
    {
        download(kaggle);
        exit(0);
    }

    // Parent process menunggu child process pertama selesai
    waitpid(pid1, &status, 0);

    // Fungsi unzip() dijalankan oleh child process kedua
    pid2 = fork();
    if (pid2 == 0)
    {
        unzip(source);
        exit(0);
    }

    // Parent process menunggu child process kedua selesai
    waitpid(pid2, &status, 0);

    // Fungsi scan() dijalankan oleh child process ketiga
    pid3 = fork();
    if (pid3 == 0)
    {
        scan();
        exit(0);
    }

    // Parent process menunggu child process ketiga selesai
    waitpid(pid3, &status, 0);

    return 0;
}

void download(char *kaggle)
{
    system(kaggle);
}

void unzip(char *source)
{
    execlp("unzip", "unzip", "-q", source, NULL);
}

void scan()
{
    system("awk -F',' 'NR>1 && $3<25 && $8>85 && $9!=\"Manchester City\" { printf(\"Name: %s\\nAge: %s\\nClub: %s\\nNationality: %s\\nPotential: %s\\nPhoto: %s\\n\\n\", $2, $3, $9, $5, $8, $4) }' FIFA23_official_data.csv");
}
